import java.util.List;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        List<String> stringList = Arrays.asList("tomato", "potato", "pepper", "onion");
        String elementToFind = "egg";
        boolean containsElement = stringList.contains(elementToFind);

        if (containsElement) {
            System.out.println("The list contains the element: " + elementToFind);
        } else {
            System.out.println("The list does not contain the element: " + elementToFind);
        }
    }
}